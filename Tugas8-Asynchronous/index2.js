var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
var i=0
function start(times){
    if(i<books.length){
        readBooksPromise(times, books[i])
        .then(hasil => start(hasil))
        .catch(error=> console.log(error))
    }
    i++
}
start(10000)