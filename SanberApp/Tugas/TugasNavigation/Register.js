import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity } from 'react-native';


export default class Login extends Component {
    render(){
        return(        
        <View style={styles.container}>            
            <View style={styles.logoContainer}>
                <Image source={require('../Logo/Hat.png')} style={styles.logo}/>
                <Text style={styles.logoContainer}>MetaNav Registration</Text>               
            </View>
            <View>
                <Text style={styles.title}>Fill your Registration:</Text>
            <TextInput 
           placeholder='Username'
           placeholderTextColor='rgba(255,255,255,0.5)'
           style={styles.input}
           />
           <TextInput
           placeholder='email'
           placeholderTextColor='rgba(255,255,255,0.5)'           
           style={styles.input}
           />
           <TextInput
           placeholder='Password'
           placeholderTextColor='rgba(255,255,255,0.5)'
           secureTextEntry
           style={styles.input}
           />
           <TextInput
           placeholder='Confirm Password'
           placeholderTextColor='rgba(255,255,255,0.5)'
           secureTextEntry
           style={styles.input}
           />
           <TouchableOpacity style={styles.loginContainer}>
               <Text style={styles.loginText}>REGISTER</Text>
           </TouchableOpacity>
           </View>
            <View style={styles.formContainer}>                
            </View>                      
        </View>
        
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'red'
    },
    logoContainer:{
        flexGrow: 1,
        flexDirection: 'row',
        color: '#fff',
        marginTop: 15,
        width: 150,
        textAlign: 'left',
        opacity: 2,
        fontSize: 22,
        marginLeft: 8
    },
    logo:{
        width:88.5,
        height:92
    },
    title:{
        color: '#fff',
        marginBottom: 15,
        width: 260,
        textAlign: 'center',
        opacity: 2,
        fontSize: 22,
        alignItems: 'center'
    },
    input: {              
        height: 50,
        backgroundColor: 'rgba(255,255,255,0.4)',
        marginBottom: 50,
        color: 'white',
        paddingHorizontal: 5,
        marginLeft: 25,
        marginRight:25,
        alignContent: 'center'
    },
    loginContainer:{
        backgroundColor: '#e74c3c',
        paddingVertical: 20,
        borderRadius:10,
        marginBottom: 50,
        marginLeft:100,
        marginRight:100,
        marginTop: 25
    },
    loginText:{
        textAlign:'center',
        color: '#ffffff',
        fontWeight: '700'
    }
    
});