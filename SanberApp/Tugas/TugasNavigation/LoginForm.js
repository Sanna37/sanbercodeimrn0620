import React, { Component } from 'react';
import { StyleSheet, TextInput, View, Text, TouchableOpacity, Button} from 'react-native';
import { StatusBar } from 'expo-status-bar';
import { SignIn } from '../Tugas15/Screen';

export default class LoginForm extends Component {
    render(){
        return(
        <View style={styles.container}>
            <StatusBar/>
           <TextInput 
           placeholder='Username or e-mail'
           placeholderTextColor='rgba(255,255,255,0.5)'
           style={styles.input}
           />
           <TextInput
           placeholder='Password'
           placeholderTextColor='rgba(255,255,255,0.5)'
           returnKeyType="go"
           secureTextEntry
           style={styles.input}
           />
           <TouchableOpacity style={styles.loginContainer}>
                <Button title='LOGIN' style={styles.loginText} onPress={() => this.props.navigation.navigate(SignIn)}/>                                    
           </TouchableOpacity>
           <Text style={styles.optionalAccount}>Don't have an account?</Text>
           <TouchableOpacity style={styles.registerButton}>
               <Text style={styles.loginText}>REGISTER</Text>
           </TouchableOpacity>
        </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 25
    },
    input: {
        height: 50,
        backgroundColor: 'rgba(255,255,255,0.4)',
        marginBottom: 15,
        color: 'white',
        paddingHorizontal: 15
    },
    loginContainer:{
        backgroundColor: '#e74c3c',
        paddingVertical: 20,
        borderRadius:10,
        marginBottom: 15,
        marginLeft:100,
        marginRight:100
    },
    loginText:{
        textAlign:'center',
        color: '#ffffff',
        fontWeight: '700'
    },
    optionalAccount:{
        textAlign: 'center',
        color: '#ffffff',
        fontSize: 20,
        marginBottom:5
    },
    registerButton:{
        backgroundColor: '#c0392b',
        paddingVertical: 20,
        borderRadius:10,
        marginTop:15,
        marginBottom:5,
        marginLeft:100,
        marginRight:100
    }
});