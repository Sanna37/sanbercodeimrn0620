import React from "react";
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import { createDrawerNavigator} from '@react-navigation/drawer';
import LoginScreen from './LoginScreen';
import AboutScreen from './AboutScreen';
import LoginForm from './LoginForm';
import SkillScreen from './SkillScreen'
import Register from './Register'


const Tabs = createBottomTabNavigator();
const HomeStack= createStackNavigator();
const SearchStack= createStackNavigator();

const HomeStackScreen=() => (
  <HomeStack.Navigator>
    <HomeStack.Screen name='Log In' component={LoginScreen}/>
    <HomeStack.Screen name='About' component={AboutScreen}/>
    <HomeStack.Screen name='User Skill' component={SkillScreen}/>
  </HomeStack.Navigator>
);

// const SearchStackScreen=() => (
//   <SearchStack.Navigator>
//     <SearchStack.Screen name='Search' component={Search}/>
//     <SearchStack.Screen name='Search2' component={Search2}/>
//   </SearchStack.Navigator>
// );

// const ProfileStack = createStackNavigator();
// const ProfileStackScreen = ()=>(
//   <ProfileStack.Navigator>
//    <ProfileStack.Screen name='Profile' component={Profile}/>
//   </ProfileStack.Navigator>
// );

const TabsScreen = ()=>(
  <Tabs.Navigator>
    <Tabs.Screen name='Awal' component={AboutScreen}/>
    <Tabs.Screen name='Search' component={"SkillScreen"}/>
  </Tabs.Navigator>
);

const Drawer = createDrawerNavigator();

export default ()=> (
  <NavigationContainer>
    <Drawer.Navigator>
      <Drawer.Screen name='About' component={'AboutScreen'}/>
      <Drawer.Screen name='Profile' component={"SkillScreen"}/>
    </Drawer.Navigator>
    
  </NavigationContainer>
)