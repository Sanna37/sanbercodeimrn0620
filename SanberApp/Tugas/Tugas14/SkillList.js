import React, { Component } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class SkillList extends Component {
    
    render(){
        let skill = this.props.skill
        
        return(
            <View style={styles.container}>
                <View style={styles.skillContent}>
                    <Icon name={skill.iconName} size={100} color='red' marginLeft={10}/>
                    <View style={styles.skillText}>
                        <Text style={styles.nameText}>{skill.skillName}</Text>
                        <Text style={styles.categoryText}>{skill.categoryName}</Text>
                        
                    </View>
                    <Text style={styles.percentage}>{skill.percentageProgress}</Text>
                    <Icon name='chevron-right' size={95} color='red'/>
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        padding:10,
        backgroundColor: '#fff',
        marginTop: 10,
        marginLeft:4,
        opacity: 0.8,
        borderRadius:10
    },
    skillContent:{
        flexDirection:'row',
        justifyContent:'space-evenly',
        alignItems:'center'
    },
    skillText:{
        flexDirection:'column',
        marginLeft: 20,
        marginRight: 20
    },
    nameText:{
        fontWeight:'bold',
        fontSize:25,
        color:'red'
    },
    categoryText:{
        color:'red',
        fontWeight:'500',
        fontSize:15
    },
    percentage:{
        justifyContent:'center',
        fontSize:40,
        marginLeft:10,
        color:'red'
    }
})