import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, FlatList, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import SkillList from './SkillList';
import skillData from './skillData.json'

export default class SkillScreen extends Component {
    render(){
        return(
            <ScrollView contentContainerStyle={{flexGrow: 1}}>
            <View style={styles.container}>
                <View style={styles.logoContainer}>
                <Image source={require('../Logo/gim2.png')} style={styles.logo}/>
                <Text style={styles.AppName}>MetaNav</Text>
                </View>

                <View style={styles.user}>
                    <Icon style={styles.userIcon} name='account-circle' size={55}/>
                    <View style={styles.userName}>
                        <Text style={{color:'white', fontSize:18, marginLeft:5}}>Hi,</Text>
                    <Text style={styles.Name}>Sanna Tonk</Text>
                    </View>
                </View>
                <View style={styles.skillText}>
                    <Text style={{fontSize:40, fontWeight:'bold', color: '#fff', marginLeft:5}}>Skill</Text>
                    <View style={{borderBottomColor: 'white',borderBottomWidth: 3, opacity: 0.5, marginLeft:5}}/>
                    <View style={styles.criteria}>
                        <Text style={styles.criteriaText}>Library/Framework</Text>
                        <Text style={styles.criteriaText}>Pemrograman</Text>
                        <Text style={styles.criteriaText}>Technology</Text>
                    </View>
                    
                    <View style={styles.body}>                        
                        <FlatList
                        style={{flexGrow:1}}
                        contentContainerStyle={{paddingTop:8}}
                        data={skillData.items}
                        renderItem={(skill)=><SkillList skill={skill.item}/>}
                        keyExtractor={(item)=>item.id.toString()}
                        />                    
                    </View>
                    
                </View>
            </View>
            </ScrollView>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'red'
    },
    logoContainer:{
        flexDirection: 'column',
        alignItems: 'flex-end',
        justifyContent: 'flex-start',
        padding: 5,
        width: -100
    },
    logo:{
        width:110,
        height:90
    },
    AppName:{
        fontSize:20,
        fontWeight: 'bold',
        marginRight:10,
        color: '#fff'
    },
    user:{
        flexDirection:'row'        
    },
    userIcon:{
        marginLeft: 10,
        color: '#fff',
        opacity: 0.7
    },
    userName:{
        flexDirection: 'column'
    },
    Name:{
        color:'#fff',
        marginLeft: 5,
        fontSize:22
    },
    skillText:{
        padding:4,
        marginRight:4    
    },
    criteria:{
        flexDirection:'row',
        alignItems:'center'
    },
    criteriaText:{
        flex:1,
        backgroundColor: '#fff',
        textAlign:'center',
        padding: 4,
        marginLeft: 4,
        marginTop: 5,
        opacity:0.7,
        fontWeight:'bold',
        borderRadius: 40,
        color:'red'
    },
    body:{
        
    }
});