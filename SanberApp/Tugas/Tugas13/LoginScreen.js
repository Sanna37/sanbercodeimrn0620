import React, { Component } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import LoginForm from './LoginForm';


export default class Login extends Component {
    render(){
        return(
        <View style={styles.container}>
            <View style={styles.logoContainer}>
                <Image source={require('../Logo/Hat.png')} style={styles.logo}/>
                <Text style={styles.title}>Welcome to MetaNav. Wanna steal some heart?</Text>
            </View>
            <View style={styles.formContainer}>
                <LoginForm />
            </View>
        </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'red'
    },
    logoContainer:{
        alignItems: 'center',
        justifyContent: 'center',
        flexGrow: 1
    },
    logo:{
        width:88.5,
        height:92
    },
    title:{
        color: '#fff',
        marginTop: 15,
        width: 260,
        textAlign: 'center',
        opacity: 2,
        fontSize: 22
    }
    
});