import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, StatusBar } from 'react-native';

export default class AboutScreen extends Component {
    render(){
        return(
            <View style={styles.container}>
                <StatusBar style='auto'/>
                <View style={styles.about}>
                    <Text style={styles.aboutText}>About Me</Text>
                    <Image source={require('../Logo/41.jpg')} style={{width:200, height:200, borderRadius:100}}/>                    
                </View>
                <View style={styles.portofolio}>
                    <Text style={{fontSize:26, marginTop:10, color:'#fff',}}>Portofolio</Text>
                <Image source={require('../Logo/gitlab.png')}style={styles.gitlabLogo}/>
                <Text style={{fontSize:18, marginTop:10, color:'#fff',}}>@Sanna37</Text>
                </View>
                <View style={styles.socialAccount}>
                    <View style={styles.Account}>
                    <Image source={require('../Logo/Instagram.png')} style={styles.accountLogo}/>
                    <Text style={styles.accountName}>@Sanna_Tonk</Text>
                    </View>
                    <View style={styles.Account}>
                    <Image source={require('../Logo/Facebook.png')} style={styles.accountLogo}/>
                    <Text style={styles.accountName}>Sanna Tonk</Text>
                    </View>
                    <View style={styles.Account}>
                    <Image source={require('../Logo/Twitter.png')} style={styles.accountLogo}/>
                    <Text style={styles.accountName}>@sanna37</Text>
                    </View>
                </View>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'red'
    },
    about:{
        flexGrow: 1,        
        alignItems: 'center',
        marginTop: 60,
    },
    aboutText:{
        fontSize: 40,
        fontWeight: '300'
    },
    portofolio:{
        height: 180,
        backgroundColor:'rgba(255,255,255,0.5)',
        marginBottom:20,
        marginLeft:20,
        marginRight:20,
        borderRadius:30,
        alignItems:'center'
    },
    gitlabLogo:{
        height:60,
        width:60,
        marginTop:20
    },
    socialAccount:{
        backgroundColor:'rgba(255,255,255,0.5)',
        height: 180,
        marginBottom:50,
        marginLeft:20,
        marginRight:20,
        borderRadius:30,
        justifyContent:'space-around',
        flexDirection: 'row'
    },
    Account:{
        alignItems: 'center',
        justifyContent: 'center'
    },
    accountLogo:{
        height:60,
        width:60,
        justifyContent:'center'
    },
    accountName: {
        fontSize: 18,
        color: '#fff',
        paddingTop:2
    }
});