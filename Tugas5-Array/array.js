console.log('No. 1 \n============')
function range(startNum, finishNum) {
    var hasil1 = []
    if(!startNum||!finishNum){
        return -1
    } else {
        if (startNum < finishNum){
            for (var i= startNum; i<=finishNum; i++){
                hasil1.push(i)
            }
        } else if (startNum>finishNum){
            for(var a= startNum; a>=finishNum; a--){
                hasil1.push(a)
            }
        }
    }
    return hasil1
}
console.log(range(1,10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54,50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1

console.log('\n \nNo. 2 \n============')
function rangeWithStep(startNum, finishNum,step) {
    var hasil1 = []
    if(!startNum||!finishNum){
        return -1
    } else {
        if (startNum < finishNum){
            for (var i= startNum; i<=finishNum; i+=step){
                hasil1.push(i)
            }
        } else if (startNum>finishNum){
            for(var a= startNum; a>=finishNum; a-=step){
                hasil1.push(a)
            }
        }
    }
    return hasil1
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]

console.log('\n \nNo. 3 \n============')
function sum1(startNum, finishNum,step){  
    var hasil3 = []
    if(!step){
        step = 1;
    }
    if(!startNum && !finishNum){
        return 0
    }
    if(!startNum||!finishNum){
        return 1
    } 
    
    else {
        if (startNum < finishNum){
            for (var i= startNum; i<=finishNum; i+=step){
                hasil3.push(i)
            }
        } else if (startNum>finishNum){
            for(var a= startNum; a>=finishNum; a-=step){
                hasil3.push(a)
            }
        }
    }
    
    var total =  0;
    for(var j=0;j<hasil3.length;j++){
        if(isNaN(hasil3[j])){
            continue;
        }
        total += hasil3[j];
    }
    return total;
}

console.log(sum1(1,10)) // 55
console.log(sum1(5, 50, 2)) // 621
console.log(sum1(15, 10)) // 75
console.log(sum1(20, 10, 2)) // 90
console.log(sum1(1)) // 1
console.log(sum1()) // 0

console.log('\n \nNo. 4 \n============')

function dataHandling(data){
    for(var i=0; i<data.length; i++){
        for(var j= 0; j<data[i].length; j++){
            if (j==0){
                var id = data[i][j];
            }else if (j==1){
                var name= data[i][j];
            }else if (j==2){
                var place= data[i][j];
            }else if (j==3){
                var date= data[i][j];
            }else{
                var hobby= data[i][j]
            }
        }
        console.log(`Nomor ID: ${id} \nNama: ${name} \nTTL: ${place},${date} \nHobi: ${hobby}`)
    }

}
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
dataHandling(input)
//Maaf gak bisa mikir lagi kak

console.log('\n \nNo. 5 \n============')
function balikKata(kata) {
    var stringBaru = "";
    for (var i = kata.length - 1; i >= 0; i--) {
        stringBaru += kata[i];
    }
    return stringBaru;
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I

console.log('\n \nNo. 6 \n============')
/*maaf kak ngeliat soal tantangannya aku udah pusing wkwkwk. kayaknya berhubungan dengan
nomor 4 juga. Mudah2an ada sesi live lagi ya, kalau bisa senin... butuh banget jawabannya
var data= ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]
function datahandling2(biodata){
    var biodata=;

}*/