console.log('Tugas 9 - ES6\n Nomor 1 \n==========')
const golden = () => {
    console.log('this is golden!!')
    return()=>{
    }
}

golden()


console.log('\n \n Nomor 2 \n==========')
const newFunction = function literal(firstName, lastName){
    return {
        firstName,
        lastName,
        fullName: ()=>{
            console.log(firstName+' '+lastName)
            return
        }
    }
}
newFunction("William", "Imoh").fullName()


console.log('\n \n Nomor 3 \n==========')
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
  const {firstName, lastName, destination, occupation, spell} = newObject;

console.log(firstName);
console.log(lastName);


console.log('\n \n Nomor 4 \n==========')
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)

console.log('\n \n Nomor 5 \n==========')
const planet = "earth"
const view = "glass"
var before = `Lorem ${view}dolor sit amet,  
consectetur adipiscing elit ${planet} do eiusmod
tempor incididunt ut labore et dolore magna aliqua.
Ut enim ad minim veniam`
 
// Driver Code
console.log(before)