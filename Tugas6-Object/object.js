console.log('Tugas 6 -OBJECT \n \n \n No. 1 \n============')
function arrayToObject(arr) {
    var now = new Date()
    var thisYear = now.getFullYear()
    var obj = {}
    for(var i = 0; i < arr.length; i++) {
        for(var j = 0; j < arr[i].length; j++) 
            if(j==0){
                obj.firstname = arr[i][j];
            }else if (j==1){
                obj.lastname = arr[i][j];
            }else if (j==2){
                obj.gender = arr[i][j];
            }else if (j==3){
                obj.age = thisYear-arr[i][j];
            }else if (arr[i][!j]){
                'invalid birth date'
            }
        console.log(obj)
    }
}
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) //Masih bingung kenapa nilai Invalid gak masuk

console.log('\n \n \n \n No. 2 \n============')
/* Maaf udah gak paham lagi soalnya kak*/
function shoppingTime(memberId, money) {
  // you can only write your code here!
}
 
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log('\n \n \n \n No. 3 \n============')
//Ini juga udah gak bisa konsentrasi lagi
//Kalau bisa tugasnya jangan terlalu kompleks kak, untuk pemula agak membingungkan jadinya
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    //your code here
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]