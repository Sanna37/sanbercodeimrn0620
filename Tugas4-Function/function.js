console.log("TUGAS 4 - FUNCTION \n\n\n No.1\n")
function teriak(){
    return'Halo Sanbers'
}
 
console.log(teriak()) // "Halo Sanbers!"
console.log('==================================================================\n \n')

console.log('No.2\n')
function kalikan(num1, num2){
    return num1 * num2
}
var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48 
console.log('==================================================================\n \n')

console.log('No.3\n')
function introduce(name, age, address, hobby){
    return 'Nama saya ' + name +', umur saya '+ age+' tahun, alamat saya di '+ address+', dan saya punya hobbi yaitu '+hobby
}
 
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)
// Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!"
console.log('==================================================================\n \n')