console.log('--CLASS-- \n No. 1 \n============')
class Animal {
    constructor(name){
        this._name = name;
        this._coldBlooded = false;
        this._legs = 4;
        
    }
    get name(){
        return this._name
    }
    get legs(){
        return this._legs
    }
    get cold_blooded(){
        return this._coldBlooded
    }
    set name(i){
        this._name = i
    }
    set legs(i){
        this._legs = i
    }
}

var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

class Ape extends Animal{
    constructor(name){
        super(name);
        }
    }
    function yell(){        
        if (name=ape){
            yell="Auoooo!"
            }
}    
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"

class Frog extends Animal{
    constructor(name){
        super(name);
        function yell(){        
        if (name=frog){
            yell="hop hop"
            }
        }
    }
} 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop"
//Materinya masih belum paham kak jadi masih kesulitan rasanya


console.log('\n \n \n \n No. 2 \n============')
class Clock {
    constructor({ template }) {
      this.template = template;
    }
  
    render() {
      var date = new Date();
  
      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      var output = this.template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    stop() {
      clearInterval(this.timer);
    }
  
    start() {
      this.render();
      this.timer = setInterval(this.render.bind(this), 1000);
    }
  }
var clock = new Clock({template: 'h:m:s'});
clock.start(); 