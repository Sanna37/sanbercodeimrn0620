console.log('1. Tugas if else')//Nomor 1
//Untuk peran penyihir
var nama = " "
var peran = " "
if (nama == " ") {
    console.log('Nama harus diisi!!')
} else if (nama == 'Sanna' && peran == ' ') {
    console.log('Halo Sanna, pilih peranmu untuk memulai game')
} else if (nama == 'Sanna' && peran == 'penyihir') {
    console.log('Halo Penyihir Sanna, kamu dapat melihat siapa yang menjadi werewolf!')
}//input nama " " dan peran " "

var nama = "Sanna"
var peran = " "
if (nama == " ") {
    console.log('Nama harus diisi!!')
} else if (nama == 'Sanna' && peran == ' ') {
    console.log('Halo Sanna, pilih peranmu untuk memulai game')
} else if (nama == 'Sanna' && peran == 'penyihir') {
    console.log('Halo Penyihir Sanna, kamu dapat melihat siapa yang menjadi werewolf!')
}//input nama "Sanna" dan peran " "

var nama = "Sanna"
var peran = "penyihir"
if (nama == " ") {
    console.log('Nama harus diisi!!')
} else if (nama == 'Sanna' && peran == ' ') {
    console.log('Halo Sanna, pilih peranmu untuk memulai game')
} else if (nama == 'Sanna' && peran == 'penyihir') {
    console.log('Selamat datang di dunia warewolf, Sanna')
    console.log('Halo Penyihir Sanna, kamu dapat melihat siapa yang menjadi werewolf!')
}//input nama "Sanna" dan peran "penyihir"

var nama1 = "Justine"
var peran1 = "Guard"
if (nama1 == " ") {
    console.log('Nama harus diisi!!')
} else if (nama1 == 'Justine' && peran1 == ' ') {
    console.log('Halo Justine, pilih peranmu untuk memulai game')
} else if (nama1 == 'Justine' && peran1 == 'Guard') {
    console.log('Selamat datang di dunia warewolf, Justine')
    console.log('Halo Guard Justine, kamu akan membantu melindungi temanmu dari serangan werewolf!')
}// input nama "Justine" dan peran "Guard"

var nama = "Roxy"
var peran = "Warewolf"
if (nama == " ") {
    console.log('Nama harus diisi!!')
} else if (nama == 'Roxy' && peran == ' ') {
    console.log('Halo Roxy, pilih peranmu untuk memulai game')
} else if (nama == 'Roxy' && peran == 'Warewolf') {
    console.log('Selamat datang di dunia warewolf, Roxy')
    console.log('Halo Warewolf Roxy, kamu akan memakan mangsa setiap malam!')
}// input nama "Roxy" dan peran "Warewolf"

console.log(" ")
console.log("2. Tugas switch case")
var tanggal = 1;
var bulan = 3;
var tahun = 1998;
switch(bulan) {
    case 1: {bulan = 'Januari'; break; }
    case 2: {bulan = 'Februari'; break; }
    case 3: {bulan = 'Maret'; break; }
    case 4: {bulan = 'April'; break; }
    case 5: {bulan = 'Mei'; break; }
    case 6: {bulan = 'Juni'; break; }
    case 7: {bulan = 'Juli'; break; }
    case 8: {bulan = 'Agustus'; break; }
    case 9: {bulan = 'September'; break; }
    case 10: {bulan = 'Oktober'; break; }
    case 11: {bulan = 'November'; break; }
    case 12: {bulan = 'Desember'; break; }
}
console.log(tanggal +"" ,bulan +"" ,tahun);