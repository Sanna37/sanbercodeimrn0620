console.log('No. 1: LOOPING-while')
console.log("LOOPING PERTAMA")
var genap = 2
while(genap <= 20) {
    console.log(genap, " - I love Coding")
    genap+= 2;
}
console.log('LOOPING KEDUA')
var genap1 = 20
while(genap1 > 0) {
    console.log(genap1, " - I will become a mobile developer")
    genap1 -=2;
}

console.log('')//memulai soal nomor 2
console.log('No. 2: LOOPING-for');
for(var i= 1; i<=20; i++){
    if(i%3==0 && i%2== 1){
        console.log(i + " - I love Coding")
    } else if(i%2== 0){
        console.log(i + ' - Berkualitas')
    } else {console.log(i+ " - Santai")}
}

console.log('')//Memulai soal nomor 3
console.log('No. 3: Membuat persegi panjang')
for(var i=1; i<5; i++){
    console.log("########")
}

console.log('')//Memulai soal nomor 4
console.log('No. 4: Membuat tangga')
var tangga= 7
var jadi=''
for(var k=0; k<=tangga; k++){
    for(var l=0; l<k; l++){
        jadi += '#'
    }
    jadi+= '\n'
}
console.log(jadi)

console.log('')//Memulai soal nomor 5
console.log('No. 5: Membuat papan catur')
for(var i=1; i<=8; i++){
    if(i%2==0){
        console.log(' # # # #')
    } else {console.log('# # # # ')}
}